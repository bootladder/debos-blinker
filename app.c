#include "System.h"
#include "AppSystemCheckpoint.h"
#include <stdint.h>
#include <stdbool.h>
#include "my_memcpy.h"

#include "Run_App.h"

//NECESSARY FOR LINKER TO BUILD BINARY CORRECTLY
__attribute__ ((used, section(".appvectortable")))
const char hello123[30] = "hello12345";
/*************************************************************** 
   Application Header and Checkpoint Pointer

   At the moment just a pointer located at 0x2000
   It points to the Application Function, and is called by the system
   Fwd Decl of app function necessary
*/

  void app(void);

  #define APP_HEADER_SECTION __attribute__ ((section(".app_header")))
  APP_HEADER_SECTION void (*apppointer)(void) = &app;

  system_checkpoint_pointer_t * mycheckpointpointer;

/*************************************************************** 
   Application Function

   This function is called by the system while(1)
   This Hello World App only calls the checkpoint function
*/

static int onetime;

void app(void)
{
  if( onetime != 0xdead ){
    onetime = 0xdead;

    //Init and Call the checkpoint function
    System_Checkpoint_Init();

    Init_App();
    return;
  }

  else
	Run_App();//Todo, Write the App!

  return;
}
/***************************************************************/
/***************************************************************/
//dummy main.  use it to get the app to compile and not optimze stuff
int main(void)
{
  app();
  if( apppointer)
    return 1;
  return 0;
}
