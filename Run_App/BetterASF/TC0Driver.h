typedef void (*tmr_callback_t)(void);
void TC0Driver_SetCallback(tmr_callback_t cb);
void TC0Driver_Init(void);
void TC0Driver_EnableInterrupts(void);
void TC0Driver_DisableInterrupts(void);
