#include "Run_App.h"
#include "AppSystemCheckpoint.h"

#include "sysTimer.h"
#include "LED_App.h"
#include "my_memcpy.h"
#include "system.h"

#define CHECKPOINT_FLAG_LED 0x10
#define CHECKPOINT_FLAG_PRINTMSG 0x8
#define CHECKPOINT_FLAG_PRINTADDR 0x2

static void _blink_handler(struct SYS_Timer_t *timer)
{
    AppSystemWriteCheckpointBuffer((uint8_t*)"ARRRRRR   \n", 20);
    AppSystemCheckpoint(0x30303A33, 0x2|0x8|CHECKPOINT_FLAG_LED);
LED_Blink_B(50);
(void)timer;
}

void Init_App(void)
{
	static uint8_t init_string[20];
	my_memcpy(init_string, (uint8_t*)"Blinker(X.X.X)      \n", 20);
	init_string[8]  = '0' + MAJOR_VERSION_NUMBER;
	init_string[10] = '0' + MINOR_VERSION_NUMBER;
	init_string[12] = '0' + BUILD_VERSION_NUMBER;
    AppSystemWriteCheckpointBuffer(init_string, 20);
    AppSystemCheckpoint(0x30303A33, 0x2|0x8);

	system_init();
    SYS_TimerInit();
	LED_Init();

static SYS_Timer_t blinktimer;
blinktimer.interval = 1000;
blinktimer.mode = SYS_TIMER_PERIODIC_MODE;
blinktimer.handler = _blink_handler;
SYS_TimerStart(&blinktimer);
}


void Run_App(void)
{
    SYS_TimerTaskHandler();
    return;
}
