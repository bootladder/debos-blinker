#include <stdint.h>
void AppSystemCheckpoint(uint32_t addr, uint8_t flags);
void AppSystemWriteCheckpointBuffer(uint8_t * buf, uint8_t len);
void System_Checkpoint_Init(void);

extern uint8_t checkpointmessagebuffer[32];
